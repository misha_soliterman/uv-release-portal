import { Route } from 'vue-router';

import { LOCAL_STORAGE_KEY_GITLAB_ACCESS_TOKEN } from '@/utility/local_storage';
import GitlabApi, { GitlabUser } from '@/api/gitlab';

type State = 'initial' | 'loading' | 'ready';

export class GitlabState {
  private _api: GitlabApi | null = null;
  private _user: GitlabUser | null = null;
  private _initialized: Promise<boolean>;
  private _state: State = 'initial';

  constructor() {
    this._initialized = this
      .applyAccessToken(localStorage.getItem(LOCAL_STORAGE_KEY_GITLAB_ACCESS_TOKEN))
      .then(() => true);
  }

  get state() {
    return this._state;
  }

  get user() {
    return this._user;
  }

  async use() {
    await this._initialized;

    if (this._api) {
      return this._api;

    } else {
      throw new Error('ログインしていません。');
    }
  }

  async loginBegin() {
    const redirectUri = encodeURIComponent(`${location.origin}/uv-release-portal`);
    location.href = 'https://gitlab.com/oauth/authorize?' +
      `client_id=${process.env.VUE_APP_GITLAB_APP_ID}&` +
      `redirect_uri=${redirectUri}&` +
      'response_type=token&' +
      'scope=read_repository+api';

    return true;
  }

  async loginComplete(route: Route) {
    const params = route.path.slice(1).split('&').map((pair) => pair.split('='));
    const accessTokenParam = params.find(([key, value]) => key === 'access_token');
    const accessToken = accessTokenParam ? accessTokenParam[1] : undefined;
    return !!accessToken ? this.applyAccessToken(accessToken) : false;
  }

  async logout() {
    return this.applyAccessToken(null);
  }

  private async applyAccessToken(accessToken: string | null) {
    if (this._state === 'loading') {
      return false;
    }

    if (accessToken) {
      const api = new GitlabApi(accessToken);
      this._state = 'loading';

      try {
        const user = await api.getMe();
        this._api = api;
        this._user = user;
        localStorage.setItem(LOCAL_STORAGE_KEY_GITLAB_ACCESS_TOKEN, accessToken);
        return true;

      } catch (error) {
        return false;

      } finally {
        this._state = 'ready';
      }

    } else {
      localStorage.removeItem(LOCAL_STORAGE_KEY_GITLAB_ACCESS_TOKEN);
      this._user = null;
      this._api = null;
      this._state = 'ready';
      return true;
    }
  }
}
