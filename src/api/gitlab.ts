import Axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import camelCase from 'camelcase-keys';
import snakeCase from 'snakecase-keys';
import _ from 'lodash';

export interface GitlabUser {
  id: number;
  username: string;
}

export interface GitlabProject {
  id: number;
  name: string;
}

export interface GitlabMilestone {
  id: number;
  title: string;
}

export interface GitlabNote {
  id: number;
  body: string;
  createdAt: string;
}

export interface GitlabNotesAnnotated {
  notes: GitlabNote[];
}

export interface GitlabLabel {
  id: number;
  name: string;
  color?: string;
}

export interface GitlabLabelEvent {
  id: number;
  title: string;
  createdAt: string;
  action: 'add' | 'remove';
  label: GitlabLabel;
}

export interface GitlabLabelEventsAnnotated {
  labelEvents: GitlabLabelEvent[];
}

export interface GitlabIssue {
  id: number;
  iid: number;
  title: string;
  createdAt: string;
  closedAt?: string;
  labels: string[];
}

export default class GitlabApi {

  private client: AxiosInstance;

  constructor(accessToken: string) {
    this.client = Axios.create({
      baseURL: 'https://gitlab.com/api/v4',
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    this.client.interceptors.request.use((request) => {
      if (request.data) {
        request.data = snakeCase(request.data);
      }

      return request;
    });

    this.client.interceptors.response.use((response) => {
      if (response.data) {
        response.data = camelCase(response.data);
      }

      return response;
    });
  }

  async getMe() {
    return this.client.get('/user').then((response) => {
      return response.data as GitlabUser;
    });
  }

  async getProject(projectId: number) {
    return this.client.get(`/projects/${projectId}`).then((response) => {
      return response.data as GitlabProject;
    });
  }

  async getProjectLabels(projectId: number) {
    return this.client.get(`/projects/${projectId}/labels`).then((response) => {
      return response.data as GitlabLabel[];
    });
  }

  async getProjectMilestones(projectId: number) {
    return this.getPaginated<GitlabMilestone>(`/projects/${projectId}/milestones`);
  }

  async getProjectMilestone(projectId: number, milestoneId: number) {
    return this.client.get(`/projects/${projectId}/milestones/${milestoneId}`).then((response) => {
      return response.data as GitlabMilestone;
    });
  }

  async getProjectMilestoneIssues(projectId: number, milestoneId: number) {
    return this.getPaginated<GitlabIssue>(`/projects/${projectId}/milestones/${milestoneId}/issues`);
  }

  async getProjectIssueNotes<I extends GitlabIssue>(projectId: number, issues: I[]) {
    const allNotes = await Promise.all(issues.map((issue) => {
      const url = `/projects/${projectId}/issues/${issue.iid}/notes`;
      return this.getPaginated<GitlabNote>(url);
    }));

    return _.zipWith(issues, allNotes, (issue, notes) => {
      return {
        ...issue,
        notes,
      } as I & GitlabNotesAnnotated;
    });
  }

  async getProjectIssueLabelEvents<I extends GitlabIssue>(projectId: number, issues: I[]) {
    const allLabelsEvents = await Promise.all(issues.map((issue) => {
      const url = `/projects/${projectId}/issues/${issue.iid}/resource_label_events`;

      // ラベルを削除するとなくなるので、そのイベントを外す。
      return this.getPaginated<GitlabLabelEvent & { label?: GitlabLabel }>(url).then((events) => {
        return events.filter((event) => !!event.label);
      });
    }));

    return _.zipWith(issues, allLabelsEvents, (issue, labelEvents) => {
      return {
        ...issue,
        labelEvents,
      } as I & GitlabLabelEventsAnnotated;
    });
  }

  private async getPaginated<V>(url: string, config: AxiosRequestConfig = {}): Promise<V[]> {
    if (!config.params) {
      config.params = {};
    }

    const initialResponse = await this.client.get(url, {
      ...config,
      params: {
        ...config.params,
        ...snakeCase({
          perPage: 100,
        }),
      },
    });

    const totalPages = parseInt(initialResponse.headers['x-total-pages'], 10) || 1;

    if (totalPages === 1) {
      return initialResponse.data;
    }

    const requests = _.range(2, totalPages + 1).map((page) => {
      return this.client.get(url, {
        ...config,
        params: {
          ...config.params,
          ...snakeCase({
            perPage: 100,
            page,
          }),
        },
      });
    });

    const responses = await Promise.all(requests);

    return responses.reduce((items, response) => {
      items.push(...response.data);
      return items;
    }, initialResponse.data);
  }
}
