import _ from 'lodash';
import Vue from 'vue';
import VueRouter from 'vue-router';

const LandingPage = () => import('@/pages/LandingPage.vue');
const ProjectPage = () => import('@/pages/ProjectPage.vue');

const router = new VueRouter({
  base: 'uv-release-portal',
  mode: 'hash',
  routes: [
    {
      path: '/projects/:projectId/milestones/:milestoneId',
      component: ProjectPage,
      props: (route) => {
        return {
          projectId: parseInt(route.params.projectId),
          milestoneId: parseInt(route.params.milestoneId),
          labelIds: _.map(route.query.labels, _.parseInt),
        };
      },
    },
    {
      path: '/**',
      component: LandingPage,
    },
  ],
});

Vue.use(VueRouter);
export default router;
